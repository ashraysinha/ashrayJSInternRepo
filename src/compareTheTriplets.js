function compareTriplets (a, b) {
  const returnArray = [0, 0]
  let i
  for (i = 0; i < 3; i++) {
    if (a[i] > b[i]) { returnArray[0] = returnArray[0] + 1 }
    if (a[i] < b[i]) { returnArray[1] = returnArray[1] + 1 }
  }
  return returnArray
}

module.exports = compareTriplets
