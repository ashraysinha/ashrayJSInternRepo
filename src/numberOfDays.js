const formatDate = (date) => {
  let dateArray = date.split('-')
  dateArray = dateArray.map((num) => {
    return parseInt(num)
  })
  return dateArray
}

const leapYears = (date) => {
  let year = date[2]
  const month = date[1]
  if (month <= 2) {
    year = year - 1
  }
  return Math.floor(year / 4 - year / 100 + year / 400)
}

const getDiff = (dateOne, dateTwo) => {
  const monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  const dateArrayOne = formatDate(dateOne)
  const dateArrayTwo = formatDate(dateTwo)

  // Days before date 1
  let daysBeforeDateOne = dateArrayOne[2] * 365 + dateArrayOne[0]

  for (let i = 0; i < dateArrayOne[1]; i++) {
    daysBeforeDateOne = daysBeforeDateOne + monthDays[i]
  }
  daysBeforeDateOne = daysBeforeDateOne + leapYears(dateArrayOne)

  // days before date 2
  let daysBeforeDateTwo = dateArrayTwo[2] * 365 + dateArrayTwo[0]

  for (let i = 0; i < dateArrayTwo[1]; i++) {
    daysBeforeDateTwo = daysBeforeDateTwo + monthDays[i]
  }
  daysBeforeDateTwo = daysBeforeDateTwo + leapYears(dateArrayTwo)
  const difference = daysBeforeDateTwo - daysBeforeDateOne
  return difference
}

module.exports = getDiff
