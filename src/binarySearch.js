const binarySearchHelper = (array, start, end, toSearch) => {
  if (start > end) { return -1 }
  const mid = end - (end - start) / 2
  if (toSearch === array[mid]) { return mid } else if (toSearch > array[mid]) { return binarySearchHelper(array, mid + 1, end, toSearch) } else { return binarySearchHelper(array, start, mid - 1, toSearch) }
}

const binarySearch = (array, toSearch) => {
  if (!Array.isArray(array) || !array.length || isNaN(toSearch)) { return 'error in input parameters' }

  if (array.some(num => isNaN(num))) { return 'error in input parameters' }

  const end = array.length
  const start = 0
  return binarySearchHelper(array, start, end, toSearch)
}
// console.log(binarySearch([5,7,11,12,'twenty',27],2))
module.exports = binarySearch
