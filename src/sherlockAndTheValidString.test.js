const { test } = require('@jest/globals')
const isValidString = require('./sherlockAndTheValidString')
const stringOne = 'aabbccd'
const stringTwo = 'abc'
const stringThree = 'aabccd'
const stringFour = 'aabbcd'
const stringFive = 'abcaabbcdaabbccd'
const stringSix = 'abcaabbcdaabccd'

describe('should return whether string is valid or not', () => {
  test('Should return validity of string', () => {
    expect(isValidString(stringOne)).toBe('YES')
  })
  test('Should return validity of string', () => {
    expect(isValidString(stringTwo)).toBe('YES')
  })
  test('Should return validity of string', () => {
    expect(isValidString(stringThree)).toBe('NO')
  })
  test('Should return validity of string', () => {
    expect(isValidString(stringFour)).toBe('NO')
  })
  test('Should return validity of string', () => {
    expect(isValidString(stringFive)).toBe('NO')
  })
  test('Should return validity of string', () => {
    expect(isValidString(stringSix)).toBe('NO')
  })
})
