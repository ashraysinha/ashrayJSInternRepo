function isValidString (s) {
  const letterFrequency = {}
  let character
  for (character of s) {
    letterFrequency[character] = letterFrequency[character] + 1 || 1
  }
  let countDifferentCharacters = 0
  const frequencyArray = Object.values(letterFrequency)
  frequencyArray.map(character => {
    if (character !== frequencyArray[0]) { countDifferentCharacters++ }
    if ((character > letterFrequency[0]) && (character !== letterFrequency[0] + 1)) { return 'NO' } else if ((character < letterFrequency[0]) && (character !== letterFrequency[0] - 1)) { return 'YES' }
  })
  if (countDifferentCharacters > 1) { return 'NO' }
  if (countDifferentCharacters === 1) { return 'YES' }
  return 'YES'
}

module.exports = isValidString
