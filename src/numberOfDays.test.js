const getDiff = require('./numberOfDays')

describe('Should return number of days between two dates', () => {
  test.each([
    ['Should return days between', '01-12-2020', '18-12-2020', 17],
    ['Should return days between', '08-11-2018', '18-10-2020', 710],
    ['Should return days between', '17-01-2020', '18-12-2020', 335]
  ])('%s', (_unusedVar, dateOne, dateTwo, difference) => {
    expect(getDiff(dateOne, dateTwo)).toBe(difference)
  })
})
