const binarySearch = require('./binarySearch')

describe('Should test Binary search program for array one', () => {
  const array = [1, 2, 3, 4, 5, 6]

  test.each([
    ['Should be index of 1', 1, 0],
    ['Should be index of 2', 2, 1],
    ['Should be index of 3', 3, 2],
    ['Should be index of 4', 4, 3],
    ['Should be index of 5', 5, 4],
    ['Should be index of 6', 6, 5],
    ['Should be index of 7', 7, -1]
  ])('%s', (_unusedVar, number, index) => {
    expect(binarySearch(array, number)).toBe(index)
  })
})

describe('Should test Binary search program for array two', () => {
  const array = [5, 7, 11, 12, 20, 27]

  test.each([
    ['Should be index of 5', 5, 0],
    ['Should be index of 7', 7, 1],
    ['Should be index of 11', 11, 2],
    ['Should be index of 12', 12, 3],
    ['Should be index of 20', 20, 4],
    ['Should be index of 27', 27, 5]
  ])('%s', (_unusedVar, number, index) => {
    expect(binarySearch(array, number)).toBe(index)
  })
})

describe('Should test Binary search program for edge cases', () => {
  const arrayOne = [5, 7, 11, 12, 'twenty', 27]
  const arrayTwo = 'one'

  test('should return error in input parameters', () => {
    expect(binarySearch(arrayOne, 11)).toBe('error in input parameters')
  })

  test('should return error in input parameters', () => {
    expect(binarySearch(arrayTwo, 11)).toBe('error in input parameters')
  })

  test('should return error in input parameters', () => {
    expect(binarySearch(arrayOne, 'eleven')).toBe('error in input parameters')
  })
})
