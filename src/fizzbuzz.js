function fizzbuzz (number) {
  if (isNaN(number) || !(number > 0)) { return console.log('Invalid number') }
  let currNumber = 1
  for (currNumber; currNumber <= number; currNumber++) {
    if (currNumber % 15 === 0) {
      console.log('FizzBuzz')
    } else if (currNumber % 3 === 0) {
      console.log('Fizz')
    } else if (currNumber % 5 === 0) {
      console.log('Buzz')
    } else { console.log(currNumber.toString()) }
  }
}

module.exports = fizzbuzz
