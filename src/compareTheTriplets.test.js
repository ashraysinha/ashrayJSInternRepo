const { test } = require('@jest/globals')
const compareTriplets = require('./compareTheTriplets')

describe('Should compare score triplets', () => {
  test.each([
    ['should return [1,1]', [5, 6, 7], [3, 6, 10], [1, 1]],
    ['should return [1,1]', [1, 2, 3], [3, 2, 1], [1, 1]],
    ['should return [2,1]', [17, 28, 30], [99, 16, 8], [2, 1]]
  ])('%s', (_unusedVar, aliceScores, bobScores, scores) => {
    expect(compareTriplets(aliceScores, bobScores)).toStrictEqual(scores)
  })
})
