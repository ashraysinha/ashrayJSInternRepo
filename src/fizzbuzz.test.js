const fizzbuzz = require('./fizzbuzz')

describe('Should test fizzbuzz program for given tests', () => {
  test('should log correct values acc to fizzbuzz logic', () => {
    const spy = jest.spyOn(console, 'log')
    const timesCalled = 20
    fizzbuzz(timesCalled)
    expect(spy).toHaveBeenCalledTimes(timesCalled)
    expect(spy.mock.calls[0][0]).toBe("1")
    expect(spy.mock.calls[14][0]).toBe("FizzBuzz")
    expect(spy.mock.calls[8][0]).toBe("Fizz")
    expect(spy.mock.calls[9][0]).toBe("Buzz")
  })
})

describe('Should test fizzbuzz program for edge cases', () => {
  test('should log Invalid number', () => {
    const spy = jest.spyOn(console, 'log')
    const timesCalled = 'dummyString'
    fizzbuzz(timesCalled)
    expect(spy).toHaveBeenCalledWith('Invalid number')
  })

  test('should log correct values acc to fizzbuzz logic', () => {
    const spy = jest.spyOn(console, 'log')
    const timesCalled = -4
    fizzbuzz(timesCalled)
    expect(spy).toHaveBeenCalledWith('Invalid number')
  })
})
