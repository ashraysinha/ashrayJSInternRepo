class Node {
  constructor (value) {
    this.value = value,
    this.next = undefined
  }
}

class LinkedList {
  constructor () {
    this.head = undefined
  }

  insertNode (number) {
    const newNode = new Node(number)
    if (this.head === undefined) {
      this.head = newNode
    } else {
      let temp = this.head
      while (temp.next !== undefined) { temp = temp.next }
      temp.next = newNode
    }
  }

  printList () {
    const listToPrint = []
    let temp = this.head
    while (temp !== undefined) {
      listToPrint.push(temp.value)
      temp = temp.next
    }
    console.log(listToPrint)
  }

  deleteNodeWithValue (value) {
    try {
      if (this.head === undefined) {
        return console.log('Empty list')
      }
      if (this.head.value === value) {
        if (this.head.next === undefined) {
          this.head = undefined
        }
        this.head = this.head.next
        return console.log(value, 'deleted')
      }
      let prev = this.head
      while (prev.next !== undefined && prev.next.value !== value) {
        prev = prev.next
      }
      if (prev.next === undefined) {
        return console.log('value not present in list')
      }
      prev.next = prev.next.next
      return console.log(value, 'deleted')
    } catch (error) {
      console.log('Could not delete')
    }
  }
}

// Case one
const listOne = new LinkedList()

listOne.insertNode(1)
listOne.insertNode(2)
listOne.insertNode(3)
listOne.insertNode(4)
listOne.insertNode(5)
listOne.insertNode(6)

console.log('=====Test 1=====')
listOne.printList()
listOne.deleteNodeWithValue(1)
listOne.printList()
listOne.deleteNodeWithValue(4)
listOne.printList()
listOne.deleteNodeWithValue(6)
listOne.printList()

// case 2
const listTwo = new LinkedList()
listTwo.insertNode(3)
listTwo.insertNode(8)
listTwo.insertNode(1)
listTwo.insertNode(7)
listTwo.insertNode(6)
listTwo.insertNode(2)

console.log('\n=====Test 2=====')
listTwo.printList()
listTwo.deleteNodeWithValue(8)
listTwo.printList()
listTwo.deleteNodeWithValue(3)
listTwo.printList()
listTwo.deleteNodeWithValue(2)
listTwo.printList()
