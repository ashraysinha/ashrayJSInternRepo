module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'standard'
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module'
  },
  rules: {
  },
  globals: {
    describe: 'writable',
    test: 'writable',
    jest: 'writable',
    expect: 'writable'
  }
}
