// jest.config.js
// Or async function
  module.exports = {

  verbose: true,
  "collectCoverageFrom": [
    "<rootDir>/src/*.{js,jsx}",
    "!**/node_modules/**",
    "!<rootDir>/src/linkedList.js"
  ],
  "coverageThreshold": {
    "global": {
      "branches": 95,
      "functions": 95,
      "lines": 95,
      "statements": 95
    }
  },
  "coverageReporters": ["json", "lcov", "text"]
}
